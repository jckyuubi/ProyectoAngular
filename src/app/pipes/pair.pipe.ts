import { Pipe, PipeTransform } from '@angular/core';
// jean
@Pipe({
    name: 'pair'
})
export class PairPipe implements PipeTransform{
    transform(value: any){
        var res = ' no es par.';
        if (value % 2 == 0) {
            res = ' si es par.';
        }
        return value + res;
    }
}