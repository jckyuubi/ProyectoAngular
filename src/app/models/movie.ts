export class Movie {
    // MODO CLÁSICO
    // public title: string;
    // public image: string;
    // public year: number;

    // constructor(title, year, image) {
    //     this.title = title;
    //     this.year = year;
    //     this.image = image;
    // }

    // MODO OPRIMIZADO TYPESCRIPT
    constructor(
        public title: string,
        public year: number,
        public image: string,
    ){}
}