import { Component } from '@angular/core';

@Component({
    selector: 'mycomponent',
    templateUrl: './mycomponent.component.html',
})
export class MyComponent{

    public title: string;
    public comment: string;
    public year: number;
    public showMovies: boolean;

    constructor () {
        this.title = 'Components Test: mycomponent1';
        this.comment = 'This is mycomponent';
        this.year = 2021;
        this.showMovies = true;
        console.log(`Component mycomponent loaded: ${this.title}, ${this.comment} in ${this.year}`);
    }

    hideMovies() {
        this.showMovies = false;
    }

    showMoviesMethod() {
        this.showMovies = true;
    }
}