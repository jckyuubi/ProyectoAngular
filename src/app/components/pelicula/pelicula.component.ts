import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Movie } from '../../models/movie';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.css']
})
export class PeliculaComponent implements OnInit {

  @Input() movie: Movie;
  @Output() MarkFavorite = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  select(event, movie) {
    this.MarkFavorite.emit({
      movie: movie,
    });
  }

}
