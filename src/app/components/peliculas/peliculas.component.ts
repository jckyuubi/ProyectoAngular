import { Component, OnInit } from '@angular/core';
import { Movie } from '../../models/movie';
import { MovieService } from '../../services/movies.service';

@Component({
  selector: 'peliculas',
  templateUrl: './peliculas.component.html',
  styleUrls: ['./peliculas.component.css'],
  providers: [MovieService],
})
export class PeliculasComponent implements OnInit {
  public title: string;
  public movies: Movie[];
  public favorite: Movie[];

  constructor(private _movieService: MovieService) {
    this.title = 'Movies Component';
    this.movies = this._movieService.getMovies();
  }

  ngOnInit(): void {
    alert(this._movieService.hiWorld())
  }

  changeTitle() {
    this.title = 'Title has changed.';
  }

  showFavorite(event) {
    this.favorite = event.movie;
  }

}
