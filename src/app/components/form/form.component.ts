import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  public user: any;
  public field: String;
  public field2: String;
  public field3: String;

  constructor() {
    this.user = {
      nombre: '',
      apellidos: '',
      bio: '',
      genero: '',
    }
  }

  ngOnInit(): void {
  }

  onSubmit(/*nombre, apellidos, bio, genero*/) {
    alert("Form sent!!");
  }
}
