import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {

  public field: String;
  public field2: String;
  public field3: String;

  constructor() { }

  ngOnInit(): void {
  }

  onClick() {
    alert("You've made click!!");
  }

  onExit() {
    alert("You've made exited!!");
  }

  onAnyKey() {
    alert("You've pressed any key!!");
  }

  onEnterKey() {
    alert("You've pressed Enter!!");
  }

}
