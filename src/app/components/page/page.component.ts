import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params} from '@angular/router';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.css']
})
export class PageComponent implements OnInit {

  public name: string;
  public lastname: string;

  constructor(private route: ActivatedRoute,
    private router: Router) {}

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.name = params.name;
      this.lastname = params.lastname;
    });
  }

  redirection() {
    this.router.navigate(['/form']);
  }

  redirectionWithParams() {
    this.router.navigate(['/page', 'Jean', 'Arévalo']);
  }
}
