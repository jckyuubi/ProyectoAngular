import { Injectable } from '@angular/core';
import { Movie } from '../models/movie';

@Injectable()
export class MovieService{
    public movies: Movie[];
    
    constructor(){
        this.movies =  [
            new Movie('Naruto', 2012, 'https://dthezntil550i.cloudfront.net/us/latest/us1612061014299310001569065/1280_960/35d84af9-27e2-4239-bb71-054ed81ef118.png'),
            new Movie('Boku no Hero Academia', 2019,'https://ytlandia.es/wp-content/uploads/2020/02/cats-39.jpg'),
            new Movie('Kimetsu no Yaiba', 2020,'https://www.latercera.com/resizer/d2WJ8gCjyW4EUDTOJGHR2E3570g=/900x600/smart/arc-anglerfish-arc2-prod-copesa.s3.amazonaws.com/public/ELFUOWWDSFHUDEEC6X3BIPNLPQ.jpg'),
            new Movie('Hunter x Hunter', 2011,'https://1.bp.blogspot.com/-603zNxgoGBM/XgvS4vQVhwI/AAAAAAAAnzU/pYdlsjhFrhsXPUFp--vEqUtqtzeZ6YkOwCLcBGAsYHQ/w1200-h630-p-k-no-nu/Manga-continua-anime-Hunter-x-Hunter.jpg'),
            new Movie('Sword Art Online', 2017,'https://nosomosnonos.com/wp-content/uploads/2020/07/SWORD-ART-ONLINE-Alicization-Lycoris.jpg'),
          ];
    }

    hiWorld() {
        return 'Hi world since an Angular service!!';
    }

    getMovies() {
        return this.movies;
    }
}
